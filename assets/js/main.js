//disable some eslint features for simplification
/* eslint-disable no-undef */
//eslint-disable-next-line
import Swiper from './plugins/swiper.min.js';

//functions on doc ready
$(() => {
    //##################Autocomplete
    //get select field
    const $interestTextInput = $('#selectInterest');
    //declare interests
    let interests;
    //do the following if input field exists (view/step-1.php)
    if ($interestTextInput.length) {
        //ajax request to get list of interests in database
        $.ajax({
            url: '../get-data/interest-autocomplete.php',
            type: 'get',
            success: (response) => {
                //parse response
                interests = JSON.parse(response);
                //initialize autocomplete
                $('#selectInterest').autocomplete({
                    lookup: interests,
                    //possible to use suggestion in callback: (suggestion) => {}
                    onSelect: () => {
                        // alert(`success ${suggestion}`);
                    },
                    lookupLimit: 5,
                    zIndex: 1,
                });
            }
        });
    }

    /*++++++++++++++++++++++++++++++++++++WORD-CLOUD++++++++++++++++++++++++++++++++++++*/
    //declare needed vars
    let cloudSvg,
        layout;
    const $selectCategory = $('#selectCategory');
    const $catList = $('.selectedCategories ul');
    let allCategories,
        filteredCategories,
        selectedCategories = [],
        categoryAutocomplete = [];
    const categoryFilter = {
        excludeNoProducts: false,
        excludeHigherFive: false,
        excludeLowerFive: false,
        specificCats: [],
    };

    //load wordcloud if the element exists
    if ($('#categoryCloud').length) {
        //ajax get categories
        $.ajax({
            url: '../get-data/word-cloud.php',
            type: 'get',
            success: (response) => {
                //parse string to json data
                allCategories = JSON.parse(response); //these are not filtered initially

                //replace &amp
                for (let i = 0; i < allCategories.length; i++) {
                    allCategories[i].value = allCategories[i].value.replace('&amp;', '&');
                }

                //built Wordcloud from data
                insertWordcloud(allCategories);
                //fill autocomplete array
                categoryAutocomplete = [...allCategories];

                //remove duplicats
                categoryAutocomplete = categoryAutocomplete.filter((cat, index, self) => {
                    const bool = self.map((e) => e.value).indexOf(cat.value) === index;
                    return bool;
                });
                //update autocomplete data
                updateCatAutocomplete();
            }
        });
    }

    //what happens when a specific cat is added to filter
    $('#addCategory').on('click', () => {
        //check if added category is valid (exists + not selected, i.e. is in autocomplete)
        if ($selectCategory.val() === '' || !categoryAutocomplete.some((cat) => {
            const bool = cat.value.toUpperCase() === $selectCategory.val().toUpperCase();
            return bool;
        })) {
            $selectCategory.attr('style', 'border-color: #ba324f;');
        } else {
            if (selectedCategories.length === 0) {
                //remove "No cats selected"
                $catList.find('li').remove();
            }
            //remove red border from previous false input
            $selectCategory.removeAttr('style');
            //get value
            const selected = $selectCategory.val();
            //add to selected cats
            selectedCategories.push(selected);
            //remove from autocomplete data
            categoryAutocomplete = categoryAutocomplete.filter((cat) => cat.value !== selected);
            //add to dom
            $catList.append(`<li class="specificCat">${selected}</li>`);
            //update autocomplete
            updateCatAutocomplete();
            //empty input field
            $selectCategory.val('');
        }
    });

    //what happens when a specific cat is removed from filter
    $(document).on('click', '.specificCat', (event) => {
        //get clicked element
        const $target = $(event.target);
        //get text -> Category name
        const catDel = $target.text();
        //remove element from dom
        $target.remove();
        //remove category from specific cats array
        selectedCategories = selectedCategories.filter((e) => e !== catDel);
        //add category to autocomplete list
        categoryAutocomplete.push({ value: catDel, data: 0 });
        //update autocomplete list
        updateCatAutocomplete();
        //add info if no cat is selected
        if (selectedCategories.length === 0) {
            $catList.append('<li>You did not select specific Categories.</li>');
        }
    });

    //what happens when the filter settings are applied
    $('#applyFilter').on('click', () => {
        categoryFilter.excludeHigherFive = !$('#greaterFive')[0].checked; //check if >5 is checked
        categoryFilter.excludeLowerFive = !$('#lowerFive')[0].checked; //check if <5 is checked
        categoryFilter.excludeNoProducts = !$('#noProds')[0].checked; //check if no products is checked
        //find categories, that match selected Categories name
        //filter all categories by name of selected categories and map the ids
        const filterIds = allCategories.filter((cat) => selectedCategories.includes(cat.value)).map((obj) => obj.data);
        //stringify array for ajax transfer later
        categoryFilter.specificCats = { ...filterIds };
        //hide popup
        $popup.removeClass('vis');
        //update WordCloud
        $.ajax({
            url: '../get-data/word-cloud.php',
            type: 'post',
            data: { data: JSON.stringify(categoryFilter) },
            success: (response) => {
                // parse string to json data
                filteredCategories = JSON.parse(response);
                //replace &amp
                for (let i = 0; i < filteredCategories.length; i++) {
                    filteredCategories[i].value = filteredCategories[i].value.replace('&amp;', '&');
                }
                //built Wordcloud from data
                insertWordcloud(filteredCategories);
            }
        });
    });

    //update autocomplete list
    function updateCatAutocomplete() {
        $('#selectCategory').autocomplete({
            lookup: categoryAutocomplete,
            //possible to use suggestion in callback: (suggestion) => {}
            onSelect: () => {
                // alert(`success ${suggestion}`);
            },
            lookupLimit: 5,
            zIndex: 1,
        });
    }

    //insert or update wordcloud
    function insertWordcloud(categories) {
        //remove old wordcloud if exists
        $('#categoryCloud svg').remove();

        //limit cat count to keep wordcloud simple
        let categoriesLimited = categories.slice(0, 99);
        //polarize for better contrast in word size
        categoriesLimited = polarize(categoriesLimited);
        // set the dimensions and margins of the graph
        const margin = {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
            },
            width = window.innerWidth - margin.left - margin.right,
            height = window.innerHeight - 282 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        cloudSvg = d3.select('#categoryCloud').append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        // Constructs a new cloud layout instance. It run an algorithm to find the position
        // of words that suits your requirements
        // Wordcloud features that are different from one word to the other must be here
        layout = d3.layout.cloud()
            .size([width, height])
            .words(categoriesLimited.map((d) => ({ text: d.value.replace('&amp;', '&'), size: d.score * 4 })))
            .padding(4)        //space between words
            /*eslint no-bitwise: ["error", { "allow": ["~"] }] */
            .rotate(() => ~~(Math.random() * 2) * 90)
            .fontSize((d) => d.size)      // font size of words
            .on('end', draw);
        layout.start();
    }

    //polarize for better contrast in wordcloud
    function polarize(words) {
        //declare min max
        let min,
            max;
        //set input as new var
        const newWords = words;
        //loop to find min and max, if there is more than 1 word
        if (words.length > 1) {
            for (let i = 0; i < newWords.length; i++) {
                if (min) {
                    if (newWords[i].score < min) {
                        min = newWords[i].score;
                    }
                } else {
                    min = newWords[i].score;
                }
                if (max) {
                    if (newWords[i].score > max) {
                        max = newWords[i].score;
                    }
                } else {
                    max = newWords[i].score;
                }
            }
            //normalize
            max -= min;
            for (let i = 0; i < newWords.length; i++) {
                let newSize = newWords[i].score - min;
                newSize /= max;
                //*20 to get readable font size
                newSize *= 20;
                //+3 for minimal font size
                newSize += 3;
                newWords[i].score = newSize;
            }
        } else {
            //only one word -> calc font size by score
            newWords[0].score = (newWords[0].score / 10) * 20 + 3;
        }
        return newWords;
    }

    // This function takes the output of 'layout' above and draw the words
    // Wordcloud features that are THE SAME from one word to the other can be here
    function draw(words) {
        cloudSvg.append('g')
            .attr('transform', `translate(${layout.size()[0] / 2},${layout.size()[1] / 2})`)
            .selectAll('text')
            .data(words)
            .enter()
            .append('text')
            .style('font-size', (d) => d.size)
            .attr('text-anchor', 'middle')
            .style('font-family', 'Poppins')
            .attr('transform', (d) => `translate(${[d.x, d.y]})rotate(${d.rotate})`)
            .text((d) => d.text);
    }

    /*+++++++++++++++++++++++++++++++++++++COUNTDOWN+++++++++++++++++++++++++++++++++++++*/
    //get time in s, get countdown dom element
    const eightMinutes = 60 * 8,
        $countdown = $('#countdown');

    //if countdown elemetn exists (view/step-3.php), start countdown
    if ($('#countdown').length) {
        startTimer(eightMinutes, $countdown);
    }

    function startTimer(duration, display) {
        let timer = duration,
            minutes,
            seconds;
        //loop until countdown is over
        setInterval(() => {
            //get mins and secs
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);
            minutes = minutes < 10 ? `0${minutes}` : minutes;
            seconds = seconds < 10 ? `0${seconds}` : seconds;

            //display them
            display.text(`${minutes}:${seconds}`);

            //decrement seconds, reload page if done
            if (--timer < 0) {
                window.location.reload();
            }
        }, 1000);
    }

    /*+++++++++++++++++++++++++++++++++++CLICK LISTENER+++++++++++++++++++++++++++++++++++*/
    //popup Toggle
    const $popup = $('.popupWrapper');
    $('#addIdea, #toggleFilter').on('click', () => {
        $popup.addClass('vis');
    });
    $('.defaultPopup .close').on('click', () => {
        $popup.removeClass('vis');
    });

    //Interest Input
    $('#inputInterest').on('click', () => {
        if ($interestTextInput.val() === '') {
            $interestTextInput.attr('style', 'border-color: #ba324f;');
        } else {
            //check if interest exists
            let insertedInterest = interests.find((interest) => interest.value === $interestTextInput.val());
            if (insertedInterest) {
                //rename object properties for cookie
                insertedInterest.interestName = insertedInterest.value;
                insertedInterest.interestId = insertedInterest.data;
                delete insertedInterest.value;
                delete insertedInterest.data;
                //set cookie and redirect
                setCookie('interest', JSON.stringify(insertedInterest), 1);
                window.location.href = './step-2.php';
            } else {
                insertedInterest = { interestName: $interestTextInput.val(), interestId: null };
                //set cookie and redirect
                setCookie('interest', JSON.stringify(insertedInterest), 1);
                window.location.href = './loading.php';
            }
        }
    });

    //start ideation (filter -> ideation screen)
    $('#startIdeation').on('click', () => {
        //set cookie with filtersettings
        setCookie('filter', JSON.stringify(categoryFilter), 1);
        //redirect to ideation view
        window.location.href = './step-3.php';
    });

    function setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        const expires = `expires=${d.toUTCString()}`;
        document.cookie = `${cname}=${cvalue};${expires};path=/`;
    }

    /*+++++++++++++++++++++++++++++++++++++++SLIDER+++++++++++++++++++++++++++++++++++++++*/
    new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        spaceBetween: 30,
        pagination: { el: '.swiper-pagination' },
        loop: false,
        effect: 'slide',

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    /*+++++++++++++++++++++++++++++++++++++SPIDER-CHART+++++++++++++++++++++++++++++++++++++*/
    const features = ['Demand', 'Supply', 'Profit'];

    if ($('#spiderChart').length) {
        const { interestId } = JSON.parse(getCookie('interest'));
        const categoryNode = $('#catTitle').attr('data-catNode');
        const { ...params } = { interestId, categoryNode };
        $.ajax({
            url: '../get-data/spider-data.php',
            type: 'get',
            data: params,
            success: (response) => {
                const spiderData = [JSON.parse(response)];

                const svg = d3.select('#spiderChart').append('svg')
                    .attr('width', 300)
                    .attr('height', 300);

                const line = d3.line()
                    .x((d) => d.x)
                    .y((d) => d.y);

                for (let i = 0; i < spiderData.length; i++) {
                    const d = spiderData[i];
                    const color = '#ba324f';
                    const coordinates = getPathCoordinates(d);

                    //draw the path element
                    svg.append('path')
                        .datum(coordinates)
                        .attr('d', line)
                        .attr('stroke-width', 3)
                        .attr('stroke', color)
                        .attr('fill', 'none')
                        .attr('stroke-opacity', 1)
                        .attr('fill-opacity', 0.5);
                }

                for (let i = 0; i < features.length; i++) {
                    const ftName = features[i];
                    const angle = (Math.PI / 2) + ((2 * Math.PI * i) / features.length);
                    const lineCoordinate = angleToCoordinate(angle, 10);
                    let labelCoordinate;
                    if (i === features.length - 1) {
                        labelCoordinate = angleToCoordinate(angle, 7);
                    } else {
                        labelCoordinate = angleToCoordinate(angle, 10);
                    }

                    //draw axis line
                    svg.append('line')
                        .attr('x1', 150)
                        .attr('y1', 150)
                        .attr('x2', lineCoordinate.x)
                        .attr('y2', lineCoordinate.y)
                        .attr('stroke', '#f8f7ff');

                    //get rotation for text
                    const dy = 150 - lineCoordinate.y;
                    const dx = 150 - lineCoordinate.x;
                    let theta = Math.atan(dy / dx);
                    theta *= 180 / Math.PI; // rads to degs

                    //draw axis label
                    svg.append('text')
                        .attr('transform', `translate(${labelCoordinate.x}, ${labelCoordinate.y}) rotate(${theta})`)
                        .attr('fill', '#f8f7ff')
                        .text(ftName);
                }
            }
        });
    }

    const radialScale = d3.scaleLinear()
        .domain([0, 10])
        .range([0, 125]);

    function angleToCoordinate(angle, value) {
        const x = Math.cos(angle) * radialScale(value);
        const y = Math.sin(angle) * radialScale(value);
        return { x: 150 + x, y: 150 - y };
    }

    function getPathCoordinates(dataPoint) {
        const coordinates = [];
        for (let i = 0; i <= features.length; i++) {
            const current = i % features.length;
            const ftName = features[current];
            const angle = (Math.PI / 2) + ((2 * Math.PI * current) / features.length);
            coordinates.push(angleToCoordinate(angle, dataPoint[ftName]));
        }
        return coordinates;
    }

    function getCookie(cname) {
        const name = `${cname}=`;
        const decodedCookie = decodeURIComponent(document.cookie);
        const ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }

    /*++++++++++++++++++++++++++++++++++++++++GETTING DATA++++++++++++++++++++++++++++++++++++++++*/

    if ($('#dataProgress').length) {
        let interest = JSON.parse(getCookie('interest'));
        interest = interest.interestName;

        //start scraping process
        //write interest to database with ajax
        $.ajax({
            url: '../write-data/insert-interest.php',
            type: 'get',
            data: { interest: JSON.stringify(interest) },
            success: (response) => {
                //get interest id from response
                const interestId = JSON.parse(response);
                //start collecting products
                collectProducts(interest, interestId, 1);
                // collectCatScore(0, interestId, interest);
            }
        });
    }

    //function to collect products
    function collectProducts(interest, interestId, page) {
        //update current action
        $('.currentAction').text('1/3 Collecting products ...');
        //build object for request
        const requestData = { interest, interestId, page };
        //request search results for interest
        $.ajax({
            url: '../scraper/search-result.php',
            type: 'get',
            data: { data: JSON.stringify(requestData) },
            success: (response) => {
                //write response in debug field
                $('#debug').html(response);
                //parse it
                const searchResultData = JSON.parse(response);
                //if error, console.error it
                if (searchResultData.error) {
                    console.error(`ERROR: ${searchResultData.error} don't worry, we try that again.`);
                }
                //check max products (200, if total results are smaller -> update max products)
                let maxProducts;
                200 > searchResultData.totalResults ? maxProducts = searchResultData.totalResults : maxProducts = 200;
                //repeat process, if 200 is not reached yet (-> request next page)
                if (searchResultData.numCollected < maxProducts) {
                    //update progress bar
                    $('#dataProgress').attr('style', `width: ${(searchResultData.numCollected / maxProducts) * 100}%`);
                    $('.loadingBar .loadingPercentage').text(`${(searchResultData.numCollected / maxProducts) * 100}%`);
                    //get next page
                    collectProducts(interest, interestId, page + 1);
                } else {
                    //products are collected -> now get categories for them
                    //reset progress bar
                    $('#dataProgress').attr('style', 'width: 0%');
                    $('.loadingBar .loadingPercentage').text('0%');
                    //update current action
                    $('.currentAction').text('2/3 Collecting categories ...');
                    //get categories
                    collectCategories(interestId);
                }
            }
        });
    }

    //function to collect categories for interest products
    function collectCategories(interestId) {
        //get categories for a product
        $.ajax({
            url: '../scraper/product-details.php',
            type: 'get',
            data: { data: JSON.stringify(interestId) },
            success: (response) => {
                //write response in debug field
                $('#debug').html(response);
                //parse it
                const searchResultData = JSON.parse(response);
                //if error -> error.log it
                if (searchResultData.error) {
                    console.error(`ERROR: ${searchResultData.error} don't worry, we try that again.`);
                }
                //repeat process if still products left
                if (searchResultData.done < 1) {
                    //update progress bar
                     $('#dataProgress').attr('style', `width: ${(searchResultData.done / searchResultData.total) * 100}%`);
                     $('.loadingBar .loadingPercentage').text(`${(searchResultData.done / searchResultData.total) * 100}%`);
                     //repeat for next product
                     collectCategories(interestId);
                } else {
                    //reset progressbar
                    $('#dataProgress').attr('style', 'width: 0%');
                    $('.loadingBar .loadingPercentage').text('0%');
                    //update current action
                    $('.currentAction').text('3/3 Getting score data ...');
                    //get average prices for cats where its missing
                    collectAvgPrices(0, interestId);
                }
            }
        });
    }

    //function to collect avg price for products where its missing
    function collectAvgPrices(totalTasks, interestId) {
        //get avg price for a cat where its missing
        $.ajax({
            url: '../scoring/get-avgprice.php',
            type: 'get',
            success: (response) => {
                //write response to debug element
                $('#debug').html(response);
                //parse it
                const searchResultData = JSON.parse(response);
                //calculate total tasks for progressbar (=100%)
                let newTotalTasks = totalTasks;
                if (newTotalTasks === 0) {
                    newTotalTasks = searchResultData.totalCats + searchResultData.withoutAvgPrice + 1;
                }
                //if error -> console.error it
                if (searchResultData.error) {
                    console.error(`ERROR: ${searchResultData.error} don't worry, we try that again.`);
                }
                //check how many are already done for progress bar
                const doneCats = totalTasks - searchResultData.totalCats - searchResultData.withoutAvgPrice;
                //repeat process if there are categories left
                if (searchResultData.withoutAvgPrice > 0) {
                    //update progressbar
                    $('#dataProgress').attr('style', `width: ${(doneCats / totalTasks) * 100}%`);
                    $('.loadingBar .loadingPercentage').text(`${(doneCats / totalTasks) * 100}%`);
                    //repeat process for remaining cats
                    collectAvgPrices(newTotalTasks, interestId);
                } else {
                    //no cats left, update processbar, dont update current action, its the same here
                    $('#dataProgress').attr('style', `width: ${(doneCats / totalTasks) * 100}%`);
                    $('.loadingBar .loadingPercentage').text(`${(doneCats / totalTasks) * 100}%`);
                    //get score data for cat x interest
                    collectCatScore(totalTasks, interestId);
                }
            }
        });
    }

    //function to get catxinterest score data
    function collectCatScore(totalTasks, interestId, interestName) {
        //build request object
        const requestData = { interestId, interestName };
        //make request to get score data for a cat for that interest
        $.ajax({
            url: '../scoring/get-cat-score.php',
            type: 'get',
            data: { data: JSON.stringify(requestData) },
            success: (response) => {
                //write response to debug
                $('#debug').html(response);
                //parse it
                const searchResultData = JSON.parse(response);
                //if there is an error -> console.error it
                if (searchResultData.error) {
                    console.error(`ERROR: ${searchResultData.error} don't worry, we try that again.`);
                }
                //calc total + done for progressbar
                const doneCats = searchResultData.categoriesDone;
                const totalCats = searchResultData.totalCats;
                //if there are cats left, repeat process
                if (doneCats < totalCats) {
                    //update process bar
                    $('#dataProgress').attr('style', `width: ${(doneCats / totalCats) * 100}%`);
                    $('.loadingBar .loadingPercentage').text(`${(doneCats / totalCats) * 100}%`);
                    //collect score data for next cat
                    collectCatScore(totalTasks, interestId, interestName);
                } else {
                    //data collection completet, calc scores
                    calculateScore();
                }
            }
        });
    }

    //function to calc scores for interest
    function calculateScore() {
        //call calc-score via ajax
        $.ajax({
            url: '../scoring/calc-score.php',
            type: 'get',
            data: { data: JSON.stringify(requestData) },
            success: () => {
                //everything complete, redirect to filter
                window.location.href = './step-2.php';
            }
        });
    }
});
