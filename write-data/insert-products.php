<?php 
/*
* Uses db-functions.php functions to insert new products into database
* $products exist, array of $product, which is an array containing all nessecary product info
* $interestId exists
*/

//include db-functions to use insertProduct() and insertProductPos()
include_once('../db-functions.php');

//loop over products
foreach($products as $product){
    //insert new product
    insertProduct($product['asin'], $product['title'], $product['imgUrl'], $product['totalReviews'], $product['rating'], $product['price'], $conn);
    //connect product with interest, using position -1 for now, because the serp position isn't used anywhere later
    insertProductPos($interestId, $product['asin'], -1, $conn);
}

//getting all products for that interest id for use where this file is included
$productsInDB = selectByQuery('select * from product as p left join interest_serp as iserp on p.asin = iserp.product_asin where iserp.interest_id = ' . $interestId, $conn);