<?php 

/*
* uses db-functions.php to add a new interest into database
* interest exists as $_GET index
*/

//include db-functions.php to use insertInterst()
include_once('../db-functions.php');

//getting interest name from $_GET
$interest = json_decode($_GET["interest"]);

//insert interest, hopefully returns ["Success", insertId]
$interestId = insertInterest($interest, $conn);
//get returned insert id
$interestId = $interestId[1];

//echo encoded interest id for use as ajax response
echo json_encode($interestId);