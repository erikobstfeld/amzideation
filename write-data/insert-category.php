<?php 

/*this file uses functions of db-functions.php to insert categories in database
* array $categories exists and is the category-tree for a single product
*/

//include db-functions.php to use insertCategory() and setProduct()
include_once('../db-functions.php');

//identify the last element of array which is the leaf in category tree and the 
//node that will be addet to product table
$lastCat = count($categories)-1;

//loop over categories in category path
foreach($categories as $pos => $category){
    //insert category to database
    insertCategory($category['node'], $category['parentNode'], $category['category'], $conn);
    //check if its leaf node
    if($pos == $lastCat){
        //save leaf node as category_node in product table
        setProduct($asin, 'category_node = "' . $category["node"] . '"', $conn);
    }
}