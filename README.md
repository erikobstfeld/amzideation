# amzideation

# description
This application implements a tool to support product ideation. It is used especially for products specialized on a certain area of interest. Therefore it uses amazon data to get products and categories to allow analogical transfer to develop new product ideas.

# demo
A demo version of the tool is published at amzideation.de/demo. Feel free to check it out, it is limited to the interests that exists in the database, crawling new products is disabled.

