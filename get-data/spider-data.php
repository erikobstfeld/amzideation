<?php

/*
*this file gets score data for spider diagram on ideation view (view/step-3.php)
*/

//include db-functions to selectByQuery()
include('../db-functions.php');

//get context for score (interest + category), should be sent with ajax as get parameters
$interestId = $_GET["interestId"];
$categoryNode = $_GET["categoryNode"];

//get score data for that context
$catQuery = "SELECT c.name, demand_score, supply_score, profit_score, score FROM category_scoring AS cs JOIN category AS c ON cs.category_node = c.node WHERE interest_id = " . $interestId . " AND category_node = " . $categoryNode;
$catData = selectByQuery($catQuery, $conn);

//build result array
$result = [];

//return score for each dimension in scoring model
$result["Demand"] = $catData[0]["demand_score"];
$result["Supply"] = $catData[0]["supply_score"];
$result["Profit"] = $catData[0]["profit_score"];

//echo json encoded results to use as ajax response
echo json_encode($result);
