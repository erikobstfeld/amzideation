<?php

/*
* This file gets data for autocomplete of interest input field (view/step-1.php)
*
*/

//include db-functions.php to use selectByQuery()
require_once('../db-functions.php');

//get all interests listed in database
$catQuery = 'SELECT interest_name AS "value", i.id AS "data" FROM interest as i join category_scoring as cs on i.id = cs.interest_id group by i.id';
$result = selectByQuery($catQuery, $conn);

//return them encoded as response for ajax request
echo(json_encode($result));


