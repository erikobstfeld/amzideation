<?php

/**
 * This file gets all nessecary data to display word-cloud (view/step-2.php) for use in ajax request
 * 
 */

//interest cookie is set on input (view/step-1.php), check if that exists
if(isset($_COOKIE["interest"]) && $_COOKIE["interest"] !== ""){
    //exists -> get interest id from cookie
    $ideationData = $_COOKIE["interest"];
    $ideationData = json_decode($ideationData, true);
    $interestId = $ideationData["interestId"];
}

//get post data from ajax request, should contain current filter data
if(isset($_POST["data"]) && $_POST["data"] !== ""){
    $parameters = json_decode($_POST["data"], true);
}

//build chained where conditions to apply filter in db query
//firstly, filter is emty. will be filled step by step
$filter = "";
//check if categories with no products should be excluded
if(isset($parameters["excludeNoProducts"]) && $parameters["excludeNoProducts"]){
    //if true insert this where condition to only get categories with product count > 0
    $filter .= "AND cs.product_count > 0 ";
}
//check if categories with a score higher 5 should be excluded
if(isset($parameters["excludeHigherFive"]) && $parameters["excludeHigherFive"]){
    //if true insert this where condition to only get categories with product score > 5 or NULL (no products -> no score)
    $filter .= "AND (cs.score < 5 OR cs.score IS NULL) ";
}
//check if categories with a score lower 5 should be excluded
if(isset($parameters["excludeLowerFive"]) && $parameters["excludeLowerFive"]){
    //if true insert this where condition to only get categories with product score < 5 or NULL (no products -> no score)
    $filter .= "AND (cs.score > 5 OR cs.score IS NULL) ";
}
//check if any specific categories are set in filter for ideation
if(isset($parameters["specificCats"]) && count($parameters["specificCats"]) > 0){
    //if true, build node IN (...) condition to only include specific cats
    $filter .= "AND c.node IN (";
    //chain nodes
    foreach($parameters["specificCats"] as $key => $node){
        $filter .= '"' . $node . '", ';
    } 
    //remove comma at last value
    $filter .= ")";
    $filter = str_replace(", )", ") ", $filter);
}

//include db-functions.php to use selectByQuery
include_once('../db-functions.php');

//select categories with filter applied
$catQuery = 'SELECT c.name AS "value", c.node AS "data", cs.score FROM category AS c JOIN category_scoring AS cs ON c.node = cs.category_node WHERE interest_id = ' . $interestId . " " . $filter . "ORDER BY cs.score DESC";
$result = selectByQuery($catQuery, $conn);

//return result json encoded for use as ajax response
echo json_encode($result);
