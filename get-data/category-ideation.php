<?php

/*
* This file gets allll the nessecary data for the ideation view (/view/step-3.php) using db-functions.php
*
*
*/

//boolean to set true if there is no further category to show
$noCategory = false;

//include db-functions to use selectByQuery()
include('../db-functions.php');

//put seen cats in filter for query -> is a category shown once, it wont show again
//check if cookie exists
if(isset($_COOKIE["seen"]) && count(json_decode($_COOKIE["seen"]))>0){
    //init where cond for db query
    $seenFilter = "AND c.node NOT IN(";
    //decode seen-array
    $seen = json_decode($_COOKIE["seen"]);
    //loop over seen cat-ids
    foreach($seen as $key => $val){
        //check if its last element for seperating comma
        if($key != count($seen)-1){
            $seenFilter .= $val . ", ";
        } else {
            $seenFilter .= $val;
        }
    }
    //close bracket for query in()
    $seenFilter .= ") ";
} else {
    //don't filter if there is no seen cookie
    $seenFilter = "";
}

//apply filter, which is set as cookie on previous view 
if(isset($_COOKIE["filter"]) && $_COOKIE["filter"] !== ""){
    //get cookie data
    $parameters = json_decode($_COOKIE["filter"], true);
}
//fist the filter string is empty, will be filled step by step
$filter = "";
//check if categories with no products should be excluded
if(isset($parameters["excludeNoProducts"]) && $parameters["excludeNoProducts"]){
    //if true insert this where condition to only get categories with product count > 0
    $filter .= "AND cs.product_count > 0 ";
}
//check if categories with a score higher 5 should be excluded
if(isset($parameters["excludeHigherFive"]) && $parameters["excludeHigherFive"]){
    //if true insert this where condition to only get categories with product score > 5 or NULL (no products -> no score)
    $filter .= "AND (cs.score < 5 OR cs.score IS NULL) ";
}
//check if categories with a score lower 5 should be excluded
if(isset($parameters["excludeLowerFive"]) && $parameters["excludeLowerFive"]){
    //if true insert this where condition to only get categories with product score < 5 or NULL (no products -> no score)
    $filter .= "AND (cs.score > 5 OR cs.score IS NULL) ";
}
//check if any specific categories are set in filter for ideation
if(isset($parameters["specificCats"]) && count($parameters["specificCats"]) > 0){
    //if true, build node IN (...) condition to only include specific cats
    $filter .= "AND c.node IN (";
    //chain nodes
    foreach($parameters["specificCats"] as $key => $node){
        $filter .= '"' . $node . '", ';
    }
    //remove comma at last value
    $filter .= ")";
    $filter = str_replace(", )", ") ", $filter);
}

//get data with filter applied, the 1 remaining cat with highest score is returned
$catQuery = "SELECT c.name, c.node, score FROM category_scoring AS cs JOIN category AS c ON cs.category_node = c.node WHERE interest_id = " . $interestId . " " . $seenFilter . $filter . "ORDER BY cs.score DESC LIMIT 1";
$catData = selectByQuery($catQuery, $conn);

//check if $catData contains value (if not, no cats left for ideation)
if(isset($catData[0]["node"]) && $catData[0]["node"] !== ""){
    //get node
    $categoryNode = $catData[0]["node"];

    //get max 3 products from different interests
    $productQuery = 'SELECT * FROM product AS p JOIN interest_serp AS ins ON p.asin = ins.product_asin WHERE category_node = "' . $categoryNode . '" AND ins.interest_id <> ' . $interestId . ' LIMIT 3';
    $products = selectByQuery($productQuery, $conn);

    //get max 3 products from interest
    $productSpecificQuery = 'SELECT * FROM product AS p JOIN interest_serp AS ins ON p.asin = ins.product_asin WHERE p.category_node = "' . $categoryNode . '" AND ins.interest_id = ' . $interestId . " LIMIT 3";
    $productsSpecific = selectByQuery($productSpecificQuery, $conn);

    //build result array
    $result = [];

    //return catName, catScore, and products
    $result["categoryName"] = $catData[0]["name"];
    $result["categoryScore"] = round($catData[0]["score"], 2);
    $result["products"] = $products;
    $result["productsForInterest"] = $productsSpecific;

    //add cat to list of seen cats
    //check if cookie exists
    if(!isset($_COOKIE["seen"])) {
        //create seen array with current category node
        $seen[0] = $categoryNode;
    } else {
        //add category node to seen array
        array_push($seen, $categoryNode);
    }
    //set cookie
    setcookie("seen", json_encode($seen), time() + (86400 * 30), "/");
}else{
    //no category left -> set boolean true for use where this is included
    $noCategory = true;
}