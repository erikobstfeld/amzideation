<?php 

//get db-functions to write score data
include("../db-functions.php");

//get interest ids, for which scoring-data exists, but score isn't calculated yet
$interestIds = selectByQuery("SELECT interest_id FROM category_scoring WHERE score IS NULL GROUP BY interest_id", $conn);

//loop over them
foreach($interestIds as $interestId){
    //get interest id (is array due to selectByQuery function)
    $interestId = $interestId["interest_id"];

    //get scoring data for that interestId
    $scoringData = selectEntity("c.node, product_count, review_count, cs.price_avg AS specificPriceAvg, c.price_avg AS priceAvg", "WHERE interest_id = " . $interestId, "category_scoring AS cs JOIN category AS c ON cs.category_node = c.node", $conn);

    //get price difference
    for($i = 0; $i<count($scoringData); $i++){
        $scoringData[$i]["priceDiff"] = $scoringData[$i]["specificPriceAvg"] - $scoringData[$i]["priceAvg"];
    }

    //get min/max
    $priceDiffExtr = getMinMax("priceDiff", $scoringData);
    $reviewCountExtr = getMinMax("review_count", $scoringData);
    $productCountExtr = getMinMax("product_count", $scoringData);

    //get specific Scores
    $scoringData = setScore("priceDiff", $priceDiffExtr, $scoringData);
    $scoringData = setScore("review_count", $reviewCountExtr, $scoringData);
    $scoringData = setScore("product_count", $productCountExtr, $scoringData, true);

    //loop over scoring data to calc score for each cat
    for($i = 0; $i<count($scoringData); $i++){
        //calc score
        $score = 1/3 * $scoringData[$i]["priceDiff"] + 1/3 * $scoringData[$i]["review_count"] + 1/3 * $scoringData[$i]["product_count"];
        $scoringData[$i]["score"] = $score;
        //write score
        setEntity("category_scoring", "demand_score = " . $scoringData[$i]["review_count"] . ", supply_score = " . $scoringData[$i]["product_count"] . ", profit_score = " . $scoringData[$i]["priceDiff"] . ", score = " . $scoringData[$i]["score"], "category_node = " . $scoringData[$i]["node"] . " AND interest_id = " . $interestId, $conn);
        if($scoringData[$i]["product_count"] == 0) {
            //set null if there are no products
            setEntity("category_scoring", "demand_score = NULL, supply_score = NULL, profit_score = NULL, score = NULL", "category_node = " . $scoringData[$i]["node"] . " AND interest_id = " . $interestId, $conn);
        }
    }



    $maxMinScore = getMinMax("score", $scoringData);
    print_r($maxMinScore);
    echo "<br>";
}

//function to get min + max val
function getMinMax($col, $data){
    //loop over data
    foreach($data as $key=>$val){
        //set max if not set
        if(isset($max)){
            //check if current value > max, if so update
            if ($val[$col] > $max){
                $max = $val[$col];
            }
        } else {
            //max not set -> initialize
            $max = $val[$col];
        }
        //check if min is set
        if(isset($min)){
            //if true, check if current value < min, if so update
            if ($val[$col] < $min){
                $min = $val[$col];
            }
        } else {
            //min not set, initialize
            $min = $val[$col];
        }
    }
    //return result array with min + max values
    $result = [];
    $result["min"] = $min;
    $result["max"] = $max;
    return $result;
}

//function to set score for 
function setScore($col, $minMax, $data, $minIsBetter = false){
    //normalized max
    $max = $minMax["max"] - $minMax["min"];
    //loop over value
    for($i = 0; $i < count($data); $i++){
        //normalize
        $score = (10*($data[$i][$col]-$minMax["min"]))/$max;
        //invert value if min is better
        if($minIsBetter){
            $score = -1*($score-10);
        }
        //save score
        $data[$i][$col]=$score;
    }
    //return data with scores
    return $data;
}