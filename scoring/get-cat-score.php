<?php 
/**
 * 
 * This file scrapes scoring information for categories x interest
 * 
 */
//include db-functions to write score data in database
include_once('../db-functions.php');

//get interest from request parameters
$interestData = json_decode($_GET["data"], true);
$interestId = $interestData["interestId"];
$interest = $interestData["interestName"];
//replace space with + to use in url
$interest = str_replace(' ', '+', $interest);

//get category for interest
$query = "select node, name from category as c join product as p on p.category_node=c.node where (c.not_valid_count = 0 OR c.not_valid_count IS NULL) AND NOT EXISTS(select cs.category_node from interest as i left join category_scoring as cs on i.id=cs.interest_id where i.id = " . $interestId . " AND c.node = cs.category_node) group
by c.node order by rand() limit 1";
$categories = selectByQuery($query, $conn);
$category = $categories[0];
//get category node
$node = $category["node"];

//init empty prices and reviews array
$prices = [];
$reviews = [];

//pagination
$page=1;
//init product count to keep track on how many products are already collected
$productCount=0;
//product limit is 50, since we want data from 50 products
$productLimit=50;
//total products is 50 initially, will be updated later
$totalProducts = $productLimit;
//init error boolean
$error=false;
//paginate while product count not 50
while($productCount < $productLimit && $productCount < $totalProducts){
//get product data from page
    $url = "http://amzideation.lokal/scraper/category-result.php?keyword=" . $interest . "&node=" . $node . "&page=" . $page;
    $serpInfo = file_get_contents("http://research.lokal/scraper/category-result.php?keyword=" . $interest . "&node=" . $node . "&page=" . $page);
    //decode response
    $serpInfo = json_decode($serpInfo, true);
    //when there are products returned, loop over them and collect prices + reviews
    if(isset($serpInfo["products"]) && $serpInfo["products"] !== ""){
        foreach($serpInfo["products"] as $key2 => $product){
            if(isset($product["price"]) && $product["price"] !== ""){
                array_push($prices, $product["price"]);
            }
            if(isset($product["totalReviews"]) && $product["totalReviews"] !== ""){
                array_push($reviews, $product["totalReviews"]);
            }
            $productCount++;
        }
    } else {
        //no products found
        //set product count 51 to stop pagination
        $productCount = 51;
        //set error true
        $error = true;
    }
    //increment page to get next one on next iteration
    $page++;
    //update total resultes
    if($totalProducts = 50){
        $totalProducts = $serpInfo["totalResults"];
    }
}

//remove empty values
$prices = array_filter($prices);
$reviews = array_filter($reviews);

if(!$error){
    //clac sum of reviews
    $sumReviews = array_sum($reviews);
    //calc abg price
    if(count($prices)>0){
        $avgPrice = array_sum($prices)/count($prices);
    } else {
        $avgPrice = "null";
    }
    //update score data
    insertCatScore($interestId, $node, $avgPrice, $sumReviews, $totalProducts, $conn);
} else {
    //no products found -> no score data
    insertCatScore($interestId, $node, 0, 0, 0, $conn);
}

//return data for progress bar
$categoriesDone = selectByQuery('SELECT * FROM category_scoring WHERE interest_id = ' . $interestId, $conn);
$totalCats = selectByQuery('SELECT c.node FROM category AS c JOIN product AS p ON c.node = p.category_node WHERE (c.not_valid_count = 0 OR c.not_valid_count IS NULL) GROUP BY c.node', $conn);
$result = [];
$result["categoriesDone"] = count($categoriesDone);
$result["totalCats"] = count($totalCats);

//echo encoded results to use as ajax response
echo json_encode($result);