<?php
/**
 * This file gets avg price for a category in general
 * 
 */

//include db-functions to write avg price to database
include_once("../db-functions.php");

//get a category without avg price
$categories = selectEntity("node, name", "WHERE c.price_avg IS NULL AND (c.not_valid_count = 0 OR c.not_valid_count IS NULL) GROUP BY c.node ORDER BY RAND() LIMIT 1", "category AS c JOIN product AS p ON c.node = p.category_node", $conn);
//if that exists, start
if(count($categories)>0){
    //get first value of array with count 1 (limited query to 1 earlier)
    $category = $categories[0];
    //get cat node
    $node = $category["node"];
    //init empty price array
    $prices = [];
    //pagination
    $page = 1;
    //product limit is 50, since we want avg price from 50 products
    $productLimit = 50;
    //total products is 50 initially, will be updated later
    $totalProducts = 50;
    //init product count to keep track on how many products are already collected
    $productCount = 0;
    //paginate until enough products are collected
    while($productCount < $productLimit && $productCount < $totalProducts){
        //get prices from page
        $prodInfo = file_get_contents("http://research.lokal/scraper/category-result.php?node=" . $node . "&page=" . $page);
        //decode response
        $prodInfo = json_decode($prodInfo, true);
        //when there are products returned, loop over them and collect prices
        if(isset($prodInfo["products"]) && $prodInfo["products"] !== ""){
            foreach($prodInfo["products"] as $key2 => $product){
                if(isset($product["price"]) && $product["price"] !== ""){
                    array_push($prices, $product["price"]);
                }
                $productCount++;
            }
        }
        //increment page to get next one on next iteration
        $page++;
        //update total products
        if($totalProducts = 50){
            $totalProducts = $prodInfo["totalResults"];
        }
    }

    //calculate avg price from prices array
    //remove empty values
    $prices = array_filter($prices);
    //get average
    $avgPrice = array_sum($prices)/count($prices);
    //set avg_price for category
    setEntity("category", "price_avg = " . $avgPrice, "node = " . $node, $conn);
} 

//return data for progressbar
$result = [];
$totalCats = selectByQuery('SELECT c.node FROM category AS c JOIN product AS p ON c.node = p.category_node WHERE (c.not_valid_count = 0 OR c.not_valid_count IS NULL) GROUP BY c.node', $conn);
$withoutAvgPrice = selectByQuery('SELECT c.node FROM category AS c JOIN product AS p ON c.node = p.category_node WHERE (c.not_valid_count = 0 OR c.not_valid_count IS NULL) AND price_avg IS NULL GROUP BY c.node', $conn);
$result["totalCats"] = count($totalCats);
$result["withoutAvgPrice"] = count($withoutAvgPrice);

//echo json encoded to use as ajax response
echo json_encode($result);