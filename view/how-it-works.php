<?php 

include("header.php");

?>

<main>
    <div class="fullHeight">
        <div class="centerContent">
            <div class="defaultRow">
                <h2 class="centerTitle">
                    <ol>
                        <li>Enter interest and hit "start".</li>
                        <li>Filter product categories for ideation to fit your needs and hit "ideate"</li>
                        <li>Ideatate on product categories until you find outstanding ideas</li>
                    </ol>
                </h2>
            </div>
        </div>
    </div>
</main>