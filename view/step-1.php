<?php 

include("header.php");

?>

<main>
    <div class="fullHeight">
        <div class="centerContent">
            <div class="defaultRow">
                <h2 class="centerTitle">
                    Input Your Interest to start Ideating.
                </h2>
            </div>
            <div class="defaultRow">
                <div class="inputTextButton">
                    <input type="text" id="selectInterest" placeholder="Your Interest">
                    <div id="inputInterest" class="centerButton btn btn-primary">Start</div>
                </div>
            </div>
        </div>
    </div>
</main>