<?php 

include("header.php");

if(isset($_COOKIE["interest"]) && $_COOKIE["interest"] != ""){
    $interestData = $_COOKIE["interest"];
    $interestData = json_decode($interestData, true);
    $interestName = $interestData["interestName"];
    $interestId = $interestData["interestId"];
} else {
    //interest not selectet, back home
    header("Location: ./step-1.php");
    die();
}

//reset seen + filter cookie
if(isset($_COOKIE["seen"]) && $_COOKIE["seen"] != ""){
    unset($_COOKIE['seen']); 
    setcookie('seen', null, -1, '/');
}
if(isset($_COOKIE["filter"]) && $_COOKIE["filter"] != ""){
    unset($_COOKIE['filter']); 
    setcookie('filter', null, -1, '/');
}

?>

<main>
    <div class="fullHeight">
        <div class="centerContent fullWidth">
            <div class="defaultRow">
                <h2 class="centerTitle">
                    Your Ideation Data is ready!
                </h2>
            </div>
            <div class="defaultRow">
                <div id="categoryCloud">

                </div>
            </div>
            <div class="defaultRow">
                <div id="toggleFilter" class="centerButton btn btn-outline-primary">Filter</div>
                <div id="startIdeation" class="centerButton btn btn-primary">Ideate</div>
            </div>
        </div>
        <div class="popupWrapper">
            <div class="defaultPopup filter">
                    <div class="popupHead">
                        <div class="close">x</div>
                        <h2 class="popupTitle">Filter</h2>
                    </div>
                    <div class="popupContent">
                        <div class="filterRow">
                            <div class="setFilter filterCol">
                                <label for="greaterFive"> Categories with a score greater then 5 <input type="checkbox" name="greaterFive" id="greaterFive" checked> <span class="checkmark"></span></label>
                                <label for="lowerFive"> Categories with a score lower then 5 <input type="checkbox" name="lowerFive" id="lowerFive" checked> <span class="checkmark"></span></label>
                                <label for="noProds"> Categories without products for <span class="interestName"><?php echo $interestName ?></span> <input type="checkbox" name="noProds" id="noProds" checked> <span class="checkmark"></span></label>
                            </div>
                            <div class="specificFilter filterCol">
                                <div class="inputTextButton">
                                    <input id="selectCategory" type="text" placeholder="A Category">
                                    <div id="addCategory" class="btn btn-outline-primary">Add</div>
                                </div>
                                <div class="selectedCategories">
                                    <ul>
                                        <li>
                                            You did not select specific Categories.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="btn btn-primary" id="applyFilter">Apply</div>
                    </div>
            </div>
        </div>
    </div>
</main>