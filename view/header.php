<head>
    <link rel="stylesheet" href="../assets/dist/css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="../assets/js/plugins/d3.v4.min.js"></script>
    <script src="../assets/js/plugins/d3.layout.cloud.js"></script>
    <script src="../assets/js/plugins/jquery.autocomplete.js"></script>
    <script type="module" src="../assets/js/main.js"></script>
</head>
<body class="<?php echo str_replace(".php", "", str_replace("/", "", $_SERVER['REQUEST_URI'])) ?>">
<?php 
    if($_SERVER['REQUEST_URI'] != "/ideate.php") {
?>
    <header>
    <div class="container">
        <div class="d-flex flex-row align-items-center justify-content-between">
            <div class="d-6">
                <a href="./step-1.php" class="logo">
                    <img src="../assets/img/logo-color.png" alt="amzideation logo">
                </a>
            </div>
            <div class="breadcrumbs">
                <ul>
                    <li>
                        <a href="./step-1.php">Start</a>
                    </li>
                    <li class="breadcrumbDivider">></li>
                    <li>
                        <a href="./step-2.php">Filter</a>
                    </li>
                    <li class="breadcrumbDivider">></li>
                    <li>
                        <a href="./step-3.php">Ideation</a>
                    </li>
                </ul>
            </div>
            <div class="d-6">
                <a href="./how-it-works.php" class="btn btn-outline-primary">how it works</a>
            </div>
        </div>
    </div>
    </header>
<?php } ?>