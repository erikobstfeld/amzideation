
<?php

/*
* This file is for scraping a amazon search result page 
* it uses get-html-page.php to grab the dom tree by url
*
*/

//include get-html-page.php to use custom_scraper_get_html()
include_once('get-html-page.php');
//include db-functions to use selectByQuery()
include_once('../db-functions.php');

//get data from ajax request
$parameters = json_decode($_GET["data"], true);
//init options for request-url parameters
$options = [];
//get interest from request parameters
$keyword = $parameters["interest"];
//replace spaces with + and concat +geschenk to get wanted products
$keyword = str_replace(' ', '+', $keyword) . "+geschenk";
//set as option k
$options["k"] = $keyword;

//check if a category node is specified in request
if(isset($parameters["node"]) && $parameters["node"] !== ""){
    //if so, get that node
    $categoryNode = $parameters["node"];
    //set it as option according to amazon url structure (rh=n:<node>)
    $options["rh"] = "n:" . $categoryNode;
}

//check if a page is specified in request
if(isset($parameters["page"]) && $parameters["page"] > 1){
    //if true && > 1, set page as option
    $page = $parameters["page"];
    $options["page"] = $page;
}

//get interest id from request params
$interestId = $parameters["interestId"];
//init empty result array
$resultArr = [];

//only do this if less than 200 prods are in db
//get current products for interst from database
$currentlyInDB = selectByQuery('select * from product as p join interest_serp as iserp on p.asin = iserp.product_asin where iserp.interest_id = ' . $interestId, $conn);
//count them
$currentlyInDB = count($currentlyInDB);
//if <200, get more
if($currentlyInDB < 200){
    //build url from options
    $url = "https://www.amazon.de/s?";
    $url .= http_build_query($options,'','&');

    //get search result dom with custom_scraper_get_html() from get-html-page.php
    $searchPage = custom_scraper_get_html($url, true);

    //EXTRACT NEEDED DATA
    //find script in dom to get search meta data
    //loop over script elelments
    foreach($searchPage->find('div#search script') as $element){
        $innerText = $element->innertext;
        //check if script contains "asinOnPageCount"
        if(strpos($innerText, "asinOnPageCount") !== false){
            //if true, get asin on page count from metadata using strpos() and substr()
            $begin = strpos($innerText, "asinOnPageCount")+17;
            $end = strpos($innerText, ",", $begin);
            $asinOnPage = substr($innerText, $begin, $end - $begin);
        }
        //check if script contains "totalResultCount"
        if(strpos($innerText, "totalResultCount") !== false && $asinOnPage !== "0"){
            //if true, get total result count from metadata using strpos() and substr()
            $begin = strpos($innerText, "totalResultCount")+18;
            $end = strpos($innerText, ",", $begin);
            $totalResultCount = substr($innerText, $begin, $end - $begin);
        }
    }

    //only do the following if asin on page not 0 (which means there are results on serp)
    if($asinOnPage !== "0"){
        //init empty products array
        $products = [];
        //find each product result in dom and loop over them
        foreach($searchPage->find('.s-main-slot.s-search-results>.s-result-item[data-asin]') as $element){
            //get asin from attribute
            $attribute = 'data-asin';
            $asin = $element->$attribute;
            //get classlist to check if its an ad ("sponsored product") we dont want that for now
            $classes = $element->class;
            //check if claslist doesnt contain "AdHolder" which is the class for sponsored product
            if($asin !== "" && strpos($classes, "AdHolder") === false){
                //if true (i.e. no sponsored product), get product information
                //init empty product arra and enter asin
                $product = [];
                $product["asin"] = $asin;

                //get title
                $title = $element->find('h2', 0)->find('span.a-text-normal', 0)->innertext;
                $product["title"] = $title;

                //get price
                if (null !== $element->find('span.a-price-whole', 0)) {
                    $price = $element->find('span.a-price-whole', 0)->innertext;
                    $product["price"] = str_replace(",", ".", $price);;
                } else {
                    //price not set (variant)
                    $product["price"] = "null";
                }

                //img_url
                $imgUrl = $element->find('img.s-image', 0)->src;
                $product["imgUrl"] = $imgUrl;

                //rating + total rev count
                if (null !== $element->find('span.a-icon-alt', 0)) {
                    //rating
                    $rating = $element->find('span.a-icon-alt', 0)->innertext;
                    $rating = explode(' ',trim($rating))[0];
                    $product["rating"] = str_replace(",", ".", $rating);
            
                    //total review count
                    $totalReviews = $element->find('.a-spacing-top-micro span.a-size-base', 0)->innertext;
                    $product["totalReviews"] = str_replace(".", "", $totalReviews);
                } else {
                    //no reviews
                    $product["rating"] = "null";
                    $product["totalReviews"] = 0;
                }

                //is prime video
                if(null !== $element->find('.a-section.a-spacing-top-small div.a-color-base .a-link-normal.a-size-base.a-text-bold', 0)){
                    $product["primeVideo"] = true;
                } else {
                    $product["primeVideo"] = false;
                }
                
                //push product to products array
                array_push($products, $product);
            }
        }

        //include insert-products.php to write products to database
        include ('../write-data/insert-products.php');

        //fill return array, add how many products where added, how many products are totally on serp 
        //and how many are already in db for that interest
        $resultArr["resultsWritten"]=count($products);
        $resultArr["totalResults"]=$totalResultCount;
        $resultArr["numCollected"]=count($productsInDB);
    } else {
        //if no products found in serp dom, return info
        $resultArr["error"]='Cannot get data from ' . $url . '.';
    }
} else {
    //if there are already 200+ prods for that interest in db, return count in db for all properties 
    $resultArr["resultsWritten"]=$currentlyInDB;
    $resultArr["totalResults"]=$currentlyInDB;
    $resultArr["numCollected"]=$currentlyInDB;
}

//echo encoded result array for use as ajax response
echo json_encode($resultArr);