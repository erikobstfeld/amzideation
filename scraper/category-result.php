
<?php

/*
* This file is very similar to search-result.php. However this is used to scrape data to calculate the score
*
*/

//include get-html-page.php to scrape any url
include_once('get-html-page.php');

//get node from ajax request parameters
$node = $_GET["node"];

//check if page is specified and get it, else page=1
if(isset($_GET["page"])){
    $page = $_GET["page"];
} else {
    $page = 1;
}

//get keyword if specified, else search for "" in category -> general category result
if(isset($_GET["keyword"])){
    $keyword = $_GET["keyword"];
    $url = "https://www.amazon.de/s?k=" . $keyword . "&rh=n%3A" . $node;
} else {
    $url = "https://www.amazon.de/s?rh=n%3A" . $node . "&fs=true";
}

//put page in url if > 1
if($page > 1){
    $url .= "&page=" . $page;
}

//get response for url (uses function getvalidresponse() to try until successful)
$searchPage = getValidResponse($url, true);

//this function uses custom_scraper_get_html until a valid response resultates (automatic retry on failure)
function getValidResponse($reqUrl, $bool){
    //get html from url with custom_scraper_get_html
    $response = custom_scraper_get_html($reqUrl, $bool);
    //check if response is error
    if(strpos($response, "Error with your request")) {
        //if so, retry
        $response = getValidResponse($reqUrl, $bool);
    }
    //return
    return $response;
}


//EXTRACT NEEDED DATA
//find script in dom to get search meta data
//loop over script elelments
foreach($searchPage->find('div#search script') as $element){
    $innerText = $element->innertext;
    //check if script contains "asinOnPageCount"
    if(strpos($innerText, "asinOnPageCount") !== false){
        //if true, get asin on page count from metadata using strpos() and substr()
        $begin = strpos($innerText, "asinOnPageCount")+17;
        $end = strpos($innerText, ",", $begin);
        $asinOnPage = substr($innerText, $begin, $end - $begin);
    }
    //check if script contains "totalResultCount"
    if(strpos($innerText, "totalResultCount") !== false && $asinOnPage !== "0"){
        //if true, get total result count from metadata using strpos() and substr()
        $begin = strpos($innerText, "totalResultCount")+18;
        $end = strpos($innerText, ",", $begin);
        $totalResultCount = substr($innerText, $begin, $end - $begin);
    }
}

//only do the following if asin on page not 0 (which means there are results on serp)
if(isset($asinOnPage) && $asinOnPage !== "0"){
    //init empty products array
    $products = [];
    //find each product result in dom and loop over them
    foreach($searchPage->find('.s-main-slot.s-search-results>.s-result-item[data-asin]') as $element){
        //get asin from attribute
        $attribute = 'data-asin';
        //get classlist to check if its an ad ("sponsored product") we dont want that for now
        $classes = $element->class;
        $asin = $element->$attribute;
        //check if claslist doesnt contain "AdHolder" which is the class for sponsored product
        if($asin !== "" && strpos($classes, "AdHolder") === false){
            //if true (i.e. no sponsored product), get product information
            //init empty product arra and enter asin
            $product = [];
            $product["asin"] = $asin;

            //get title
            $title = $element->find('h2', 0)->find('span.a-text-normal', 0)->innertext;
            $product["title"] = $title;

            //get price
            if (null !== $element->find('span.a-price-whole', 0)) {
                $price = $element->find('span.a-price-whole', 0)->innertext;
                $product["price"] = str_replace(",", ".", $price);;
            } else {
                //price not set (variant)
                $product["price"] = "null";
            }

            //img_url
            $imgUrl = $element->find('img.s-image', 0)->src;
            $product["imgUrl"] = $imgUrl;

            //rating + total rev count
            if (null !== $element->find('span.a-icon-alt', 0)) {
                //rating
                $rating = $element->find('span.a-icon-alt', 0)->innertext;
                $rating = explode(' ',trim($rating))[0];
                $product["rating"] = str_replace(",", ".", $rating);
        
                //total review count
                $totalReviews = $element->find('.a-spacing-top-micro span.a-size-base', 0)->innertext;
                $product["totalReviews"] = str_replace(".", "", $totalReviews);
            } else {
                //no reviews
                $product["rating"] = "null";
                $product["totalReviews"] = 0;
            }

            //is prime video
            if(null !== $element->find('.a-section.a-spacing-top-small div.a-color-base .a-link-normal.a-size-base.a-text-bold', 0)){
                $product["primeVideo"] = true;
            } else {
                $product["primeVideo"] = false;
            }
            //push product to products array
            array_push($products, $product);
        }
    }

    //fill return array, add products, how many are on that page and how many products are totally on serp 
    $resultArr = [];
    $resultArr["products"]=$products;
    $resultArr["resultsOnPage"]=count($products);
    $resultArr["totalResults"]=$totalResultCount;

    //echo encoded result array for use as ajax response
    echo json_encode($resultArr);
} else {
    //no products are found, echo that
    echo "No Products";
}
