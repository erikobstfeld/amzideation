<?php 

/*
* This file contains a function to scrape html data from any url
* it uses simplehtmldom to parse the string result to dom
*
*/

//include simplehtmldom parser
require_once '../simplehtmldom_1_9_1/simple_html_dom.php';

//parameters: $url (url to be scraped), $use_proxy(boolean to decide if proxy is used), $returnDom (boolean to decide on returning dom or string)
function custom_scraper_get_html($url, $use_proxy=FALSE, $returnDom=TRUE){
    //use proxy if parameter is set true
    if ($use_proxy) {
        //get scrapingbee api key from condfigs
        $configs = include('../config.php');
        $proxy_key = $configs["scrapingbeeKey"];

        // get cURL resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, 'https://app.scrapingbee.com/api/v1/?api_key=' . $proxy_key . '&url=' . urlencode($url) . '&render_js=false');

        // set method
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        // return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // send the request and save response to $response
        $response = curl_exec($ch);

        // stop if fails
        if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }

        // close curl resource to free up system resources
        curl_close($ch);

        //return $response directly, if its an error
        if(strpos($response, "Error with your request")){
            return $response;
        }

        //get dom if parameter is true
        if($returnDom){
            //get dom with str_get_html from simplehtmldom
            $html = str_get_html($response);
        } else {
            //do nothing with the response
            $html = $response;
        }
    } else {
        // do it without a proxy (not recommended on many request, ip will likely get blocked)
        $html = file_get_contents($url);

        //convert to dom if parameter is true
        if($returnDom){
            $html = str_get_html($response);
        }
    }
    //return
    return $html;
}