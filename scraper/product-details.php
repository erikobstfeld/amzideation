
<?php

/*
* This file scrapes a amazon product detail page for category information
*
* Unfortunately simplehtmldom fails, so we have to use strpos and substr to find the data manually
*/

//include db-functions and get-html-page to scrape url and write database
include_once('get-html-page.php');
include_once('../db-functions.php');

//get a product from that intre
$interestId = json_decode($_GET['data']);

//get asin for products without category information filled
$asinList = selectByQuery('SELECT p.asin FROM product AS p JOIN interest_serp AS iserp ON p.asin=iserp.product_asin WHERE p.category_node IS NULL AND iserp.interest_id = ' . $interestId . ' ORDER BY RAND() LIMIT 1', $conn);
$asin = $asinList[0]["asin"];

//build url for adp
$url = 'https://www.amazon.de/dp/' . $asin;

//scrape that url
$detailPage = custom_scraper_get_html($url, TRUE, FALSE);

//check if its prime video or no category (no category data available)
if(strpos($detailPage, 'data-feature-name="wayfinding-breadcrumbs"') == false){
    
    //add that information in array (prime/no cat, no parent)
    $categories = [];
    $categories[0]["node"] = "PrimeOrNoCat";
    $categories[0]["category"] = "PrimeOrNoCat";
    $categories[0]["parentNode"] = null;
} else {
    //category available -> read it out
    //get category breadcrumb with function getElementFromString()
    $breadcrumbs = getElementFromString($detailPage, 'data-feature-name="wayfinding-breadcrumbs">', '</ul>');

    //explode breadcrumb on breadcrumb dividers
    $breadcrumbArr = explode("a-breadcrumb-divider", $breadcrumbs);

    //loop over breadcrumb elements
    for($i = 0; $i<count($breadcrumbArr); $i++){
        //get category link element
        $categoryLink = getElementFromString($breadcrumbArr[$i], 'class="a-list-item">', '</a>');
        //set breadcrumb value empty array
        $breadcrumbArr[$i] = [];
        //check if link is actually found and not empty
        if($categoryLink !== ""){
            //get category name
            $category = getElementFromString($categoryLink, '">', '</a');
            $category = substr($category, 0, strlen($category)-strlen('</a'));
    
            //get category node
            $node = getElementFromString($categoryLink, 'node=', '">');
            $node = substr($node, 0, strlen($node)-5);
    
            //save node and name in array
            $breadcrumbArr[$i]["node"] = $node;
            $breadcrumbArr[$i]["category"] = trim($category);
            
            //check if category has parent node in category path
            if(isset($breadcrumbArr[$i-1]["node"]) && $breadcrumbArr[$i-1]["node"] !== ""){
                //true -> save that
                $breadcrumbArr[$i]["parentNode"] = $breadcrumbArr[$i-1]["node"];
            } else {
                //false -> parent = null
                $breadcrumbArr[$i]["parentNode"] = null;
            }
        }
    }
    //filter empty values and make indices numeric
    $breadcrumbArr = array_filter($breadcrumbArr);
    $breadcrumbArr = array_values($breadcrumbArr);

    //init empty result array
    $result = [];
    //categories are in breadcrumArr
    $categories = $breadcrumbArr;
}

//write categories to database + set product category
include('../write-data/insert-category.php');

//check how many products need to be scraped and how many are done
$totalProds = selectByQuery('SELECT p.asin FROM product AS p JOIN interest_serp AS iserp ON p.asin=iserp.product_asin WHERE iserp.interest_id = ' . $interestId , $conn);
$doneProds = selectByQuery('SELECT p.asin FROM product AS p JOIN interest_serp AS iserp ON p.asin=iserp.product_asin WHERE p.category_node IS NOT NULL AND iserp.interest_id = ' . $interestId , $conn);
//add that info to result (used for progressbar in loading screen view/loading.php)
$result['total'] = count($totalProds);
$result['done'] = count($doneProds);

//echo encoded result for use as ajax response
echo json_encode($result);


//function to get element from string with begin and end identifier
function getElementFromString($string, $beginIdentifier, $endIdentifier) {
    //find beginpos
    $begin = strrpos($string, $beginIdentifier);
    //find endpos
    $end = strpos($string, $endIdentifier, $begin);
    //get substring
    $element = substr($string, $begin + strlen($beginIdentifier), $end - $begin);
    //return
    return $element;
}