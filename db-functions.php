<?php 
/*
* This file contains every function used to read and write the database
*
*/



/*++++++++++++++++++++++++++++DB Connection++++++++++++++++++++++++++++*/
//load in credentials
$configs = include('config.php');

//DB Credentials
$servername = $configs["host"];
$username = $configs["user"];
$password = $configs["password"];
$dbname = $configs["dbName"];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    //connection failed, display error
    die("Connection failed: " . $conn->connect_error);
}


/*++++++++++++++++++++++++++++++++INSERTING++++++++++++++++++++++++++++++++*/

//inserting new interest
function insertInterest($interestName, $conn){
    //check if exists
    $interestQuery = 'SELECT * FROM interest WHERE interest_name = "'. $interestName . '"';
    $result = $conn->query($interestQuery);
    if($result->num_rows == 0){
        //insert new if doesnt exist
        $interestQuery = 'INSERT INTO interest (interest_name) VALUES ("' . $interestName . '")';
        if ($conn->query($interestQuery) === TRUE) {
            //success -> return id
            return ["Success", $conn->insert_id];
        } else {
            //failed -> return error info
            echo "Error", "Error: " . $interestQuery . "<br>" . $conn->error;
            return ["Error", "Error: " . $interestQuery . "<br>" . $conn->error];
        }
    } else {
        //exists -> return interest id
        $result = $result->fetch_assoc();
        return ["", $result["id"]];
    }
}

//inserting new product
function insertProduct($asin, $title, $imgUrl, $totalRev, $rating, $price, $conn){
    //check if product exists
    $productQuery = 'SELECT * FROM product WHERE asin = "'. $asin . '"';
    $result = $conn->query($productQuery);
    if($result->num_rows == 0){
        //insert new product if doesnt exist
        $productQuery = 'INSERT INTO product (asin, title, img_url, total_reviews, rating, price) VALUES ("' . $asin . '", "' . str_replace('"', '\"', $title) . '", "' . $imgUrl . '", ' . $totalRev . ', ' . $rating . ', ' . $price . ')';
        if ($conn->query($productQuery) === TRUE) {
            return ["Success", $asin];
        } else {
            //return error message on failiure
            echo  "Error", "Error: " . $productQuery . "<br>" . $conn->error;
            return ["Error", "Error: " . $productQuery . "<br>" . $conn->error];
        }
    }
}

//inserting new product x interest connection
function insertProductPos($interestId, $asin, $pos, $conn){
    //check if that entry already exists
    $interestSerpQuery = 'SELECT * FROM interest_serp WHERE product_asin = "'. $asin . '" AND interest_id = ' . $interestId;
    $result = $conn->query($interestSerpQuery);
    if($result->num_rows == 0){
        //insert new productxinterest connection if doesnt exist
        $interestSerpQuery = 'INSERT INTO interest_serp (interest_id, product_asin, position) VALUES (' . $interestId . ', "' . $asin . '", ' . $pos . ')';
        if ($conn->query($interestSerpQuery) === TRUE) {
            return ["Success", $conn->insert_id];
        } else {
            //return error message on failiure
            echo "Error", "Error: " . $interestSerpQuery . "<br>" . $conn->error;
            return ["Error", "Error: " . $interestSerpQuery . "<br>" . $conn->error];
        }
    }
}

//inserting new categories
function insertCategory($node, $parentNode, $catName, $conn){
    //check if exists
    $catQuery = 'SELECT * FROM category WHERE node = "'. $node . '"';
    $result = $conn->query($catQuery);
    if($result->num_rows == 0){
        //insert new category
        //wrap potential null value in quotes for query
        if($parentNode !== "null"){
            $parentNode = '"' . $parentNode . '"';
        }
        //inserting...
        $catQuery = 'INSERT INTO category (node, parent_node, name) VALUES ("' . $node . '", ' . $parentNode . ', "' . $catName . '")';
        if ($conn->query($catQuery) === TRUE) {
            return ["Success", $node];
        } else {
            //error messge on failure
            echo "Error", "Error: " . $catQuery . "<br>" . $conn->error;
            return ["Error", "Error: " . $catQuery . "<br>" . $conn->error];
        }
    } else {
        //exists -> return info
        return ["Exists", $node];
    }
}

//inserting catscore (interestxcategory)
function insertCatScore($interestId, $catNode, $priceAvg, $totalReviews, $totalProducts, $conn){
    //inserting...
    $catScoreQuery = 'INSERT INTO category_scoring (category_node, interest_id, product_count, review_count, price_avg) VALUES ("' . $catNode . '", ' . $interestId . ', ' . $totalProducts . ', ' . $totalReviews . ', ' . $priceAvg . ')';
    if ($conn->query($catScoreQuery) === TRUE) {
        return ["Success", $conn->insert_id];
    } else {
        //error msg on failiure
        echo "Error", "Error: " . $catScoreQuery . "<br>" . $conn->error;
        return ["Error", "Error: " . $catScoreQuery . "<br>" . $conn->error];
    }
}

/*+++++++++++++++++++++++++++++++++SETTING+++++++++++++++++++++++++++++++++*/
//setproduct
function setProduct($asin, $decl, $conn){
    //setting...
    $productQuery = 'UPDATE product SET ' . $decl . ' WHERE asin = "' . $asin . '"';
    $conn->query($productQuery);
}

//setAnything
function setEntity($table, $decl, $cond, $conn){
    //setting
    $query = 'UPDATE ' . $table . ' SET ' . $decl . ' WHERE ' . $cond;
    $conn->query($query);
}



/*++++++++++++++++++++++++++++++++SELECTING++++++++++++++++++++++++++++++++*/
//select products
function selectProducts($cols, $filerStr, $conn){
    //build query
    $productQuery = "SELECT " . $cols . " FROM product " . $filerStr;
    $result = $conn->query($productQuery);
    //if values exist -> put them in array
    if ($result->num_rows > 0) {
        // output data of each row
        $products = [];
        //push each row
        while($row = $result->fetch_assoc()) {
            $product = [];
            //push each column
            foreach($row as $key => $val){
                $product[$key] = $val;
            }
            array_push($products, $product);
        }
        //return result array
        return $products;
    } else {
        //no results -> return empty array
        return [];
    }
}

//select anything
function selectEntity($cols, $filerStr, $table, $conn){
    //build query
    $query = "SELECT " . $cols . " FROM " . $table . " " . $filerStr;
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
        // output data of each row
        $sets = [];
        //push each row in array
        while($row = $result->fetch_assoc()) {
            $set = [];
            //push each column in array
            foreach($row as $key => $val){
                $set[$key] = $val;
            }
            array_push($sets, $set);
        }
        return $sets;
    } else {
        //return empty array on no results
        return [];
    }
}

//select anything by query
function selectByQuery($query, $conn){
    //select data
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
        // output data of each row
        $sets = [];
        //push each row in array
        while($row = $result->fetch_assoc()) {
            $set = [];
            //push each column in array
            foreach($row as $key => $val){
                $set[$key] = $val;
            }
            array_push($sets, $set);
        }
        //return results
        return $sets;
    } else {
        //no results -> return empty array
        return [];
    }
}